import "./Card.css";
import DeleteIcon from "@mui/icons-material/Delete";

function Card({ results, deleteCard, cards }) {
    
  const outCard = () => {
    cards.map((card)=>(
      deleteCard(card.index)
    ))
    
  }

  return (
    <>
      {results.map((result) => (
        <div className="card-container" key={results.id}>
          <img src={result.picture.large} alt="userImege" />
          <h3>{result.name.first}</h3>
          <h3>{result.name.last}</h3>
          <div>
            <button className="delete-card" onClick={outCard}>
              <DeleteIcon style={{ color: "#FA948F" }} fontSize="small" />
            </button>
          </div>
        </div>
      ))}
    </>
  );
}

export default Card;
