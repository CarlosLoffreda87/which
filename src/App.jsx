import Card from "./Components/Card/Card";
import Header from "./Components/Header/Header";
import "./App.css";
import { useState } from "react";

function App() {
  const [cards, setCards] = useState([]);

  const createUserCard = async () => {
    //Llamada a la API
    const apiCall = "https://randomuser.me/api/";
    const response = await fetch(apiCall); 
    const { results } = await response.json();

    //Seteo del estado
    setCards([ ... cards, <Card results={results} key={results.id} deleteCard={deleteCard} cards={cards} />])
      
  
    
    //User info de ayuda en consola
    /* console.log(results); */
    console.log(cards)
  };

  const deleteAllCards = () => {
    if (cards.length > 0) {
      alert("Estás seguro de eliminar todas las Cards?");
      setCards(cards == []);
    } 
  };

  function deleteCard(cardIndex) {
    setCards(cards.splice(cardIndex))
  }


  return (
    <div className="app-container">
      <Header />
      <div className="buttons-container">
        <button className="create-button" onClick={createUserCard}>
          Create UserCard
        </button>
        <button className="delete-button" onClick={deleteAllCards}>
          Delete All
        </button>
      </div>
      <div className="cards-container">
        {cards}
      </div>
    </div>
  );
}

export default App;
